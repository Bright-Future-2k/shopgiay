<?php

use App\Shoes;
use Illuminate\Database\Seeder;

class ShoesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shoes = new Shoes();
        $shoes->name = 'Yeezy 350';
        $shoes->price = '8500000';
        $shoes->picture = 'a.jpg';
        $shoes->status = 'con hang';
        $shoes->save();
    }
}
