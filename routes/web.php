<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::prefix('shoes')->group(function (){
    Route::get('/','ShoesController@index')->name('shoes.index');
    Route::get('/create','ShoesController@create')->name('shoes.create');
    Route::post('/store','ShoesController@store')->name('shoes.store');
    Route::get('/edit/{id}','ShoesController@edit')->name('shoes.edit');
    Route::post('/update/{id}','ShoesController@update')->name('shoes.update');
    Route::get('/delete/{id}','ShoesController@delete')->name('shoes.delete');
});
