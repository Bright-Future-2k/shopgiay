@extends('dashbroad')
@section('content')
    <div class="container">
        <div class="col-12 mt-4">
            <h1 class="center">Edit Shoes</h1>
        </div>
        <div class="col-12 mt-4">
            <form method="post" enctype="multipart/form-data" action="{{ route('shoes.update', $shoes->id)}}">
                @csrf
                <div class="form-group">
                    <label>Name:</label>
                    <input name="name" type="text" class="form-control" value="{{ $shoes->name }}">
                </div>
                <div class="form-group">
                    <label>Price:</label>
                    <input name="price" type="number" class="form-control" value="{{ $shoes->price }}">
                </div>
                <div class="form-group">
                    <label>Picture:</label>
                    <input name="picture" type="file" value="{{ $shoes->picture }}">
                </div>
                <div class="form-group">
                    <label>Status:</label>
                    <select class="custom-select" name="status">
                        <option value="Stocking">Stocking - Còn hàng</option>
                        <option value="OutOfStock">Out of stock - Hết hàng</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
