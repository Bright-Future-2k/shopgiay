@extends('body')
@section('listShoes')
    <div class="container">
        <div class="row row-pb-md">
            <div class="w-100"></div>
            <div class="col-12 mt-4 mb-3">
                <a href="{{route('shoes.create')}}" type="button" class="btn btn-outline-primary">Create</a>
            </div>

            @foreach($allShoes as $shoes)
                <div class="col-lg-3 mb-4 text-center">
                    <div class="product-entry border">
                        <a href="#" class="prod-img">
                            <img src="{{asset('storage/'. $shoes->picture)}}" class="img-fluid" alt="shoes.img">
                        </a>
                        <div class="desc">
                            <h1>{{ $shoes->name }}</h1>
                            @if($shoes->status == 'Stocking')
                                <p style="color: green">{{ $shoes->status }}</p>
                            @elseif($shoes->status == 'OutOfStock')
                                <p style="color: red">{{ $shoes->status }}</p>
                            @endif
                            <span class="price">{{ $shoes->price }} VND</span>
                            <td><a href="{{ route('shoes.edit', $shoes->id) }}" type="button"
                                   class="btn btn-outline-warning">Edit</a></td>
                            <td><a href="{{ route('shoes.delete', $shoes->id) }}" type="button"
                                   class="btn btn-outline-danger" onclick="confirm('Are you sure ???')">Delete</a>
                            </td>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection



