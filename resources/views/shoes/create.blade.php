@extends('dashbroad')
@section('content')
    <div class="container">
        <div class="col-12 mt-4">
            <h1 class="center">Create Shoes</h1>
        </div>
        <div class="col-12 mt-4">
            <form method="post" enctype="multipart/form-data" action="{{ route('shoes.store')}}">
                @csrf
                <div class="form-group">
                    <label>Name:</label>
                    <input name="name" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label>Price:</label>
                    <input name="price" type="number" class="form-control">
                </div>
                <div class="form-group">
                    <label>Picture:</label>
                    <input name="picture" type="file">
                </div>
                <div class="form-group">
                    <label>Status:</label>
                    <select class="custom-select" name="status">
                        <option value="Stocking">Stocking - Còn hàng</option>
                        <option value="OutOfStock">Out of stock - Hết hàng</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
