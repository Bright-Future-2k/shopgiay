<?php

namespace App\Http\Controllers;

use App\Shoes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;


class ShoesController extends Controller
{
    public function index()
    {
        $allShoes = Shoes::all();
        return view('shoes.list', compact('allShoes'));
    }

    public function create()
    {
        return view('shoes.create');
    }

    public function store(Request $request)
    {
        $shoes = new Shoes();
        $shoes->name = $request->input('name');
        $shoes->price = $request->input('price');
        if ($request->hasFile('picture')) {
            $image = $request->file('picture');
            $path = $image->store('pictures', 'public');
            $shoes->picture = $path;
        }
        $shoes->status = $request->input('status');
        $shoes->save();

        Session::flash('Create Success!');

        return redirect()->route('shoes.index');
    }

    public function edit($id)
    {
        $shoes = Shoes::find($id);
        return view('shoes.edit', compact('shoes'));
    }

    public function update(Request $request, $id)
    {
        $shoes = Shoes::find($id);
        $shoes->name = $request->input('name');
        $shoes->price = $request->input('price');
        if ($request->hasfile('picture')) {
            $currentImg = $shoes->picture;
            if ($currentImg) {
                Storage::delete('/public/' . $currentImg);
            }
            $image = $request->file('picture');
            $path = $image->store('pictures', 'public');
            $shoes->picture = $path;
        }
        $shoes->status = $request->input('status');
        $shoes->save();

        Session::flash('Update data success!');

        return redirect()->route('shoes.index');
    }

    public function delete($id)
    {
        $shoes = Shoes::find($id);
        $shoes->delete();

        Session::flash('Delete success!');

        return redirect()->route('shoes.index');
    }
}
